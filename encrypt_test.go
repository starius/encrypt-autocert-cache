package encrypt

import (
	"bytes"
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"

	"golang.org/x/crypto/acme/autocert"
)

func TestEncryptedCache(t *testing.T) {
	tmpDir, err := os.MkdirTemp(os.TempDir(), "encrypt-autocert-cache")
	if err != nil {
		t.Fatalf("os.MkdirTemp: %v", err)
	}
	defer os.RemoveAll(tmpDir)

	dirCache := autocert.DirCache(tmpDir)
	encryptedCache, err := NewEncryptedCache(dirCache, []byte("key for testing"))
	if err != nil {
		t.Fatalf("NewEncryptedCache: %v", err)
	}

	ctx := context.Background()

	// Write data.
	r := rand.New(rand.NewSource(777))
	for i := 0; i < 100; i++ {
		key := fmt.Sprintf("key %d", i)
		value := make([]byte, r.Intn(1000))
		if _, err := r.Read(value); err != nil {
			t.Fatalf("rand.Read: %v", err)
		}
		if err := encryptedCache.Put(ctx, key, value); err != nil {
			t.Fatalf("encryptedCache.Put: %v", err)
		}
	}

	// Check data.
	r = rand.New(rand.NewSource(777))
	for i := 0; i < 100; i++ {
		key := fmt.Sprintf("key %d", i)
		wantValue := make([]byte, r.Intn(1000))
		if _, err := r.Read(wantValue); err != nil {
			t.Fatalf("rand.Read: %v", err)
		}
		gotValue, err := encryptedCache.Get(ctx, key)
		if err != nil {
			t.Fatalf("encryptedCache.Get: %v", err)
		}
		if !bytes.Equal(gotValue, wantValue) {
			t.Fatalf("value mismatch")
		}
	}

	// Detele keys.
	r = rand.New(rand.NewSource(777))
	for i := 0; i < 100; i++ {
		key := fmt.Sprintf("key %d", i)
		value := make([]byte, r.Intn(1000))
		if _, err := r.Read(value); err != nil {
			t.Fatalf("rand.Read: %v", err)
		}
		if err := encryptedCache.Delete(ctx, key); err != nil {
			t.Fatalf("encryptedCache.Delete: %v", err)
		}
		// Make sure Get returns error.
		if _, err := encryptedCache.Get(ctx, key); err == nil {
			t.Fatalf("encryptedCache.Get: want error")
		}
	}

	// Make sure the dir is empty.
	names, err := os.ReadDir(tmpDir)
	if err != nil {
		t.Fatalf("os.ReadDir: %v", err)
	}
	if len(names) != 0 {
		t.Fatalf("Wanted the directory to be empty.")
	}
}
