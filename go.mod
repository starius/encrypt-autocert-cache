module gitlab.com/starius/encrypt-autocert-cache

go 1.19

require (
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	golang.org/x/crypto v0.23.0
)

require (
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
)
